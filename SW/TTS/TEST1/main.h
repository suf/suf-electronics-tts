#pragma once
#define STEPPER_EN  8
#define STEPPER_DIR 5
#define STEPPER_STEP  2
#define LIMIT_OPEN  9
#define LIMIT_CLOSE 10

// number of steps needed after the endstop signaled in slow mode to fully open/close the lid
#define SLOW_CLOSE_STEPS 320
#define SLOW_OPEN_STEPS 320

#define THRESHOLD_IN  6
#define COMP_IN 7
#define CAP_CHARGE_OUT 13

#define TOUCH_THRESHOLD 1000
#define TOUCH_NOISE_THRESHOLD 10
#define TOUCH_DEAD_TIME 1000