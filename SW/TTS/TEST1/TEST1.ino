/*
 Name:		TEST1.ino
 Created:	12/21/2019 7:01:24 AM
 Author:	zoli
*/

#include <Arduino.h>
#include "main.h"

// the setup function runs once when you press reset or power the board
void setup()
{
	// Setup motor
	pinMode(STEPPER_EN, OUTPUT);
	digitalWrite(STEPPER_EN, LOW);
	pinMode(STEPPER_DIR, OUTPUT);
	pinMode(STEPPER_STEP, OUTPUT);
	//setup timer2 for stepping
	// interrupt at 8kHz
	TCCR2A = 0;// set entire TCCR2A register to 0
	TCCR2B = 0;// same for TCCR2B
	TCNT2 = 0;//initialize counter value to 0
	// set compare match register for 8khz increments
	OCR2A = 249;// = (16*10^6) / (8000*8) - 1 (must be <256)
	// turn on CTC mode
	TCCR2A |= (1 << WGM21);
	// Set CS21 bit for 8 prescaler
	TCCR2B |= (1 << CS21);
	// TCCR2B |= (1 << CS21) | (1 << CS20);
	// enable timer compare interrupt
	TIMSK2 |= (1 << OCIE2A);


	digitalWrite(STEPPER_DIR, 1);
	digitalWrite(STEPPER_EN, 0);
}

// the loop function runs over and over again until power down or reset
void loop()
{
  
}

ISR(TIMER2_COMPA_vect)
{
	digitalWrite(STEPPER_STEP, !digitalRead(STEPPER_STEP));
	/*
	if (slow_mode)
	{
		slow_count++;
	}
	*/
}
