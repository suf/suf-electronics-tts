#pragma once

#define STEPPER_EN  8
#define STEPPER_DIR 5
#define STEPPER_STEP  2
#define LIMIT_OPEN  9
#define LIMIT_CLOSE 10

// number of steps needed after the endstop signaled in slow mode to fully open/close the lid
#define SLOW_CLOSE_STEPS 320
#define SLOW_OPEN_STEPS 320

#define TOUCH_PIN_CHARGE 4
#define TOUCH_PIN_SENSE 6
#define TOUCH_SAMPLE_INTERVAL 250
#define TOUCH_THRESHOLD 1000
